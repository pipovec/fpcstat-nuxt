export const state = () => ({
  baseWargamingURL: 'https://api.worldoftanks.eu/wot',
  applicationId: '883ff6ceefb13177357ffea34d6fb06f',
  playerClanHistory: {}
})

export const mutations = {
  addPlayerClanHistory(state, data) {
    state.playerClanHistory = data
  }
}

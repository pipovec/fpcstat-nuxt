export const state = () => ({
  players: {}
})

export const mutations = {
  players (state, data) {
    state.players = data
  }
}
